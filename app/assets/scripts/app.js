import RevealOnScroll from './modules/RevealOnScroll';
import $ from 'jquery';

new RevealOnScroll($(".why-event-item"), "100%");
new RevealOnScroll($(".target-market-item"), "100%");
new RevealOnScroll($(".benefits-feature"), "85%");
new RevealOnScroll($(".event-item"), "100%");
new RevealOnScroll($(".cd-timeline-block"), "50%");




